const redis = require('redis');
const redisClient = redis.createClient();


module.exports.saveChatToDb = function(output){
	let date = new Date();
	date = date.toDateString();

	redisClient.lpush('chatlog', date + ' ' + output, ()=>{
		console.log(date + output);
		redisClient.ltrim('chatlog', [0, 1000]);
	});
};

module.exports.getLatestPosts = function(callback){
	redisClient.lrange('chatlog', [0, 9], callback);
};